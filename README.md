bash-aws
===

Alpine image with `bash` and `aws cli`. `jq` has been included to parse aws cli outputs. Running as user `1000`, a member of group `0`.
