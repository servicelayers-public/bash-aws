ARG BASE_IMAGE
FROM $BASE_IMAGE

ENTRYPOINT [ "bash" ]

## Multiple layers are used to increase reuse
# Add user 1000 as a member of root group
RUN adduser --system --uid 1000 --gecos "" --gid 0 --disabled-login app

RUN set -eux; \
  DEBIAN_FRONTEND=noninteractive \
  apt-get update; \
  # upgrade against vulnerabilities
  apt-get upgrade -y; \
  # apt-transport-https - https package repos
  # ca-certificates - trusted root certs
  # curl - download
  # groff - used by awscli
  # less - used by awscli
  # unzip - extract archives
  apt-get install -y --no-install-recommends \
  apt-transport-https \
  ca-certificates \
  curl \
  groff \
  less \
  unzip \
  ; \
  rm -rf /var/lib/apt/lists/*

# Direct download for version without CVEs
ARG JQ_VERSION
RUN set -eux; \
  curl -fsSL -o /usr/local/bin/jq \
  https://github.com/jqlang/jq/releases/download/jq-$JQ_VERSION/jq-linux64; \
  chmod +x /usr/local/bin/jq

# AWS CLI v2 https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-linux.html
RUN set -eux; \
  cd /tmp; \
  curl -fsSL -o awscliv2.zip https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip; \
  unzip -q awscliv2.zip; \
  ./aws/install; \
  rm -rf aws*; \
  aws --version; \
  rm -rf /tmp/.aws

USER 1000
