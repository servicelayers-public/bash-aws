# 3.0.0

* AWS CLI `aws` updated to v2 https://github.com/aws/aws-cli/blob/v2/CHANGELOG.rst#200
* Base image changed to debian-slim
